fs = require('fs');
if (process.argv.length > 2) {
    let mode = process.argv[2];
    let point;
    if (mode === "dev") {
        point = "http://localhost:5000";
        debug = "true";
        fs.writeFileSync('src/app/app.settings.ts', 'export class AppSettings {public static API_ENDPOINT= "'+point+'"; public static DEBUG='+debug+';}');     
    }
    if (mode === "prod") {
        point = "https://api.l24u.ru";
        debug = "false"
        fs.writeFileSync('src/app/app.settings.ts', 'export class AppSettings {public static API_ENDPOINT= "'+point+'"; public static DEBUG='+debug+';}');     
    }
    console.log("replace ok - " + point);
}