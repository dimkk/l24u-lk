import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalState } from '../../../global.state';
import { BaMenuService } from '../../services/baMenu/baMenu.service';

@Component({
  selector: 'ba-content-top',
  styles: [require('./baContentTop.scss')],
  template: require('./baContentTop.html'),
})
export class BaContentTop {

  public activePageTitle: string = '';

  constructor(
    private _state: GlobalState,
    private router: Router,
    private menu: BaMenuService
  ) {
    this.activePageTitle = menu.getCurrentItem().title;
    this._state.subscribe('menu.activeLink', (activeLink) => {
      if (activeLink) {
        this.activePageTitle = activeLink.title;
      }
    });
  }
}
