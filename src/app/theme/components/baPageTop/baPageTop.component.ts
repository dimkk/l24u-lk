import { Component, ViewEncapsulation } from '@angular/core';

import { GlobalState } from '../../../global.state';
import { AppSettings } from '../../../app.settings';

import { AuthService } from '../../../pages/shared/auth/auth.service';
import { IAccount } from '../../../pages/shared/auth/auth.interfaces';

@Component({
  selector: 'ba-page-top',
  styles: [require('./baPageTop.scss')],
  template: require('./baPageTop.html'),
  encapsulation: ViewEncapsulation.None
})
export class BaPageTop {

  public isScrolled: boolean = false;
  public isMenuCollapsed: boolean = false;
  public user: IAccount;

  constructor(
    private _state: GlobalState,
    private auth: AuthService
    ) {
      this.user = auth.currentUser;
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });
    this._state.subscribe('currentUser', (user) => {
      this.user = user;
    });
  }

  public toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

  public scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }

  profileClick(){
    if (AppSettings.DEBUG) {
      this.auth.getUser(true);
    }
  }

  public logout(){
    this.auth.logout();
  }
}
