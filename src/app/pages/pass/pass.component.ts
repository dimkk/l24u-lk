import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../shared/auth/auth.service';
import { AppSettings } from '../../app.settings';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';

import { GlobalState } from '../../global.state';

@Component({
  selector: 'pass',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./pass.scss')],
  template: require('./pass.html'),
})
export class Pass {

  public form: FormGroup;
  public email: AbstractControl;
  message: string;
  showCaptcha = false;

  constructor(
    fb: FormBuilder,
    private service: AuthService,
    private router: Router,
    private _state: GlobalState
  ) {


    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });
    this.email = this.form.controls['email'];
  }

  public isDebug(){
    return AppSettings.DEBUG;
  }
  public handleCorrectCaptcha($event) {
      this.service.restorePass(this.email.value, $event).subscribe(
        user => this.message = 'Почта отправлена!',
        err => this.message = 'Пользователь не найден!',
      );
  }
  handleRestore(){
    if (this.isDebug()) {
      this.service.restorePassDebug(this.email.value).subscribe(
        user => this.message = 'Почта отправлена!',
        err => this.message = 'Пользователь не найден!',
      );
    }
    else {
      this.showCaptcha = true;
    }
  }
}
