import { Routes, RouterModule }  from '@angular/router';

import { Pass } from './pass.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Pass
  }
];

export const routing = RouterModule.forChild(routes);
