import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { Pass } from './pass.component';
import { routing }       from './pass.routing';

import { ReCaptchaModule } from 'angular2-recaptcha';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
    ReCaptchaModule
  ],
  declarations: [
    Pass
  ]
})
export default class PassModule {}
