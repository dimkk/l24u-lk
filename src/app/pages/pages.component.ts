import { Component, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from './shared/auth/auth.service';
import { BaMenuService } from '../theme';
import { MENU } from '../app.menu';
import { ADMIN_MENU } from './admin/admin.menu';
import { Routes } from '@angular/router';
import { AppSettings } from '../app.settings';
import { Router } from '@angular/router';

@Component({
  selector: 'pages',
  encapsulation: ViewEncapsulation.None,
  styles: [],
  // [style.height.px]='h'
  //       id="al-cont"
  //       (window:resize)="onResize($event)"
  template: `
    <ba-sidebar></ba-sidebar>
    <ba-page-top></ba-page-top>
    <div class="al-main">
      <div class="al-content"
      >
        <ba-content-top></ba-content-top>
        <router-outlet></router-outlet>
      </div>
    </div>
    <footer class="al-footer clearfix">
      <div class="al-footer-right">Created with FURY</div>
      <div class="al-footer-main clearfix">
        <div class="al-copy">&copy; <a href="https://l24u.ru">L24u</a> 2017</div>
        <ul class="al-share clearfix">
          <li><i class="socicon socicon-facebook"></i></li>
          <li><i class="socicon socicon-vkontakte"></i></li>
        </ul>
      </div>
    </footer>
    <ba-back-top position="200"></ba-back-top>
    `
})
export class Pages {
  h;
  constructor(
    private auth: AuthService,
    private menu: BaMenuService,
    private router: Router
  ) {
    // || AppSettings.DEBUG
    if ((auth.currentUser && auth.currentUser.role === 'admin')) {
      MENU[0].children = MENU[0].children.concat(ADMIN_MENU);
      menu.updateMenuByRoutes(<Routes>MENU);
    }
  }
  ngOnInit() {
  }

  ngAfterViewChecked(){
  }
}
