import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { EmailValidator, EqualPasswordsValidator } from '../../theme/validators';
import { AuthService } from '../shared/auth/auth.service';
import { IAccount } from '../shared/auth/auth.interfaces';
import { AppSettings } from '../../app.settings';
import { GlobalState } from '../../global.state';
import {
  CanActivate, Router,
  ActivatedRoute,
  RouterStateSnapshot
}                           from '@angular/router';

@Component({
  selector: 'register',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./register.scss')],
  template: require('./register.html'),
})
export class Register {

  public form: FormGroup;
  public name: AbstractControl;
  // public code: AbstractControl;
  public email: AbstractControl;
  public password: AbstractControl;
  public repeatPassword: AbstractControl;
  public passwords: FormGroup;
  public error: string;
  public submitted: boolean = false;
  showCaptcha: boolean;
  disableButton: boolean;
  buttonMessage: string = 'Зарегистрироваться';
  constructor(
   fb: FormBuilder,
   private auth: AuthService,
   private router: Router,
   private _state: GlobalState,
   ) {
     this.error = '';
    this.form = fb.group({
      'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      // 'code': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      'passwords': fb.group({
        'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
        'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
      }, {validator: EqualPasswordsValidator.validate('password', 'repeatPassword')})
    });

    this.name = this.form.controls['name'];
    // this.code = this.form.controls['code'];
    this.email = this.form.controls['email'];
    this.passwords = <FormGroup> this.form.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.repeatPassword = this.passwords.controls['repeatPassword'];

    if (AppSettings.DEBUG) {
      this.name.setValue('test');
      this.email.setValue('test@test.test');
      this.password.setValue('test');
      this.repeatPassword.setValue('test');
    }
    // let code = this.getParameterByName('code', location.hash);
    let email = decodeURIComponent(this.getParameterByName('email', location.hash));
    // if (code) {
    //   this.code.setValue(code);
    // }
    if (email !== 'null') {
      this.email.setValue(email);
    }
  }
  public handleCorrectCaptcha($event) {
    this.error = '';
    this.showCaptcha = false;
    this.disableButton = true;
    this.buttonMessage = 'Регистрируемся...';
    if (this.form.valid) {
        let user = {
        name : this.name.value,
        email : this.email.value,
        password : this.password.value,
        recaptcha: $event,
        // inviteCode: this.code.value
      } as IAccount;
      this.auth.registerUser(user).subscribe(
        user => {
          this.auth.setUserAndGoTo(user);
          this.buttonMessage = 'Успех!';
        },
        error => {
          this.showCaptcha = true;
          this.buttonMessage = 'Ошибка';
          this.disableButton = false;
        }
      );
    };
  }
   getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
}

