import { Injectable } from '@angular/core';
import { AppSettings } from '../../app.settings';

@Injectable()
export class RegisterService {
  getInit: RequestInit = { method: 'GET',
               headers: new Headers({
                 'Access-Control-Allow-Origin': '*'
               }),
               mode: 'cors',
               cache: 'default' };

  sendData(userData: INewUser): Promise<Response> {
    return window.fetch(AppSettings.API_ENDPOINT + '/api/v1/register', this.getInit);
  }
}

export interface INewUser {
  name: string;
  email: string;
  password: string;
}
