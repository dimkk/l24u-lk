import { Observable } from 'rxjs';
export function handleLoginError(error: Response | any) {
  // In a real world app, we might use a remote logging infrastructure
  let errMsg: string;
  if (error.json()) {
    const body = error.json() || '';
    const errObj = body.error;
    let err = '';
    if (errObj) {
      err = `${errObj.status} - ${errObj.name}: ${errObj.message} [${errObj.code}]`;
    } else {
      err = JSON.stringify(errObj);
    }
    errMsg = err;
  } else {
    errMsg = error.message ? error.message : error.toString();
  }
  //console.error(errMsg);
  return Observable.throw(errMsg ? errMsg : error);
};

export function handleLogin2Error(error: Response | any) {
  // In a real world app, we might use a remote logging infrastructure
  let errMsg: string;
  if (error.json()) {
    const body = error.json() || '';
    errMsg = body.message;
  //console.error(errMsg);
  }
  return Observable.throw(errMsg ? errMsg : error);
};
export function handleRegisterError(error: Response | any) {
  // In a real world app, we might use a remote logging infrastructure
  let errMsg: string;
  if (error.json()) {
    let err = '';
    const body = error.json() || '';
    if (body.name === "ValidationError") {
      if (body.errors.email) {
        err = body.errors.email.message + ' ';
      }
      if (body.errors.name) {
        err += body.errors.name.message + ' ';
      }
      if (body.errors.password) {
        err += body.errors.password.message + ' ';
      }
    }
    if (body.error) {
      err = 'Капча не решена!';
    }
    errMsg = err;
  } else {
    errMsg = error.message ? error.message : error.toString();
  }
  //console.error(errMsg);
  return Observable.throw(errMsg ? errMsg : error);
};

