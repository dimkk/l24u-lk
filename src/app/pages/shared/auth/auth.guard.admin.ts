import { Injectable }     from '@angular/core';
import { AuthService } from './auth.service';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';

@Injectable()
export class AuthGuardAdmin implements CanActivate {
  /**
   *
   */
  constructor(
    private auth: AuthService,
    private router: Router
  ) {

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    ) {
    return this.auth.getUserObs().then((user) => {
       if (this.auth.currentUser && this.auth.currentUser.role === 'admin') {
            return true;
          }
        this.router.navigate(['/pages/dasboard/manage']);
        return false;
    })
    .catch(() => {
      this.router.navigate(['/pages/dasboard/manage']);
        return false;
    });
  }
};
