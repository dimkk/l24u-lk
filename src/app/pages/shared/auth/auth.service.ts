import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AppSettings } from '../../../app.settings';
import { CookieService } from 'angular2-cookie/core';
import { HttpClient } from '../HttpClient';
import * as rx from 'rxjs';
import * as handler from '../errorHandler';
import { Router } from '@angular/router';
import { GlobalState } from '../../../global.state';
import { LocalStorageService } from 'angular-2-local-storage';
import { IAccount } from './auth.interfaces';

@Injectable()
export class AuthService {
  public currentUser: IAccount;

  REQUEST_URL: string = AppSettings.API_ENDPOINT;
  REQUEST_URL_ME: string = this.REQUEST_URL + '/api/users/me';
  REQUEST_URL_AUTH: string = this.REQUEST_URL + '/auth/local';
  REQUEST_URL_AUTH_RESTORE: string = this.REQUEST_URL_AUTH + '/restore';
  REQUEST_URL_REGISTER: string = this.REQUEST_URL + '/api/users';
  /**
   *
   */
  constructor(
    private cookie: CookieService,
    private http: HttpClient,
    private router: Router,
    private state: GlobalState,
    private storage: LocalStorageService
        ) {
  }

  registerUser(userData: IAccount, dontUseCookie?: boolean): rx.Observable<IAccount> {
      return this.http.post(this.REQUEST_URL_REGISTER, userData)
        .map((res: any) => {
            //TODO сделать expire
            if (!dontUseCookie)
                this.cookie.put('token', res.json().token);
        })
        .catch(handler.handleRegisterError)
        .flatMap(() => {
            return this.getUserObs();
        });
  }

  login(email, password, recaptcha) {
      return this.http.post(this.REQUEST_URL_AUTH, {email, password, recaptcha})
        .map((res) => {
            //TODO сделать expire
            this.cookie.put('token', res.json().token);
        })
        .catch(handler.handleLogin2Error)
        .flatMap(() => {
            return this.getUserObs();
        });
  }

  restorePass(email, recaptcha) {
      return this.http.post(this.REQUEST_URL_AUTH_RESTORE, {email, recaptcha})
        .map((res) => {
            //TODO сделать expire
            //this.cookie.put('token', res.json().token);
        });
  }

  restorePassDebug(email) {
      return this.http.post(this.REQUEST_URL_AUTH_RESTORE + 'Debug', {email})
        .map((res) => {
            //TODO сделать expire
            //this.cookie.put('token', res.json().token);
        });
  }

  logout(){
      this.cookie.remove('token');
      this.currentUser = null;
  }

  getUserObs(): Promise<IAccount> {
      return new Promise((resolve, reject) => {
        this.http.get(this.REQUEST_URL_ME)
        .toPromise()
        .then((res) => {
            let body = res.json();
            this.state.notifyDataChanged('currentUser', body);
            this.currentUser = body;
            resolve(body as IAccount);
        })
        .catch((err) => {
          reject(err);
        });
      });
  }
//   tryRestoreFromStorage(){
//       if (this.storage.get<IAccount>('loggedInUser')) {
//           this.currentUser = this.storage.get('loggedInUser');
//       }
//   }
  getUser(dontNavigate?){
    if (!this.currentUser) {
      this.getUserObs()
        .then((user) => {
          this.setUserAndGoTo(user, dontNavigate);
        })
        .catch(() => {
          this.router.navigate(['/login']);
        });
    };
  }
  setUser(){
    if (AppSettings.DEBUG) {
      return this.getUserObs();
    }
  }
  setUserAndGoTo(user: IAccount, dontNavigate?) {
    if (!user || !user.role) console.error('user null or user.role null');
    this.state.notifyDataChanged('currentUser', user);
    this.currentUser = user;
    if (AppSettings.DEBUG) {
      //console.log(user);
    }
    if (!dontNavigate) {
      if (user.role === 'admin') {
      this.router.navigate(['/pages/admin/accounts']);
      }
      if (user.role === 'user') {
        this.router.navigate(['/pages/dashboard/manage']);
      }
    }
  }
}
