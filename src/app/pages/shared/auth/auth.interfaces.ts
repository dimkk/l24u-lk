export interface IAccount {
  name: string;
  email: string;
  role?: string;
  provider?: string;
  _id?: string;
  password: string;
  l2Id?: number;
  invitesLeft?: number;
  createdAt?: Date;
  updatedAt?: Date;
  recaptcha?: string;
}
