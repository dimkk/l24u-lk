import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { CookieService } from 'angular2-cookie/core';

@Injectable()
export class HttpClient {

  constructor(private http: Http, private cookie: CookieService) {}

  createAuthorizationHeader(headers: Headers) {
      if (this.cookie.get('token')) {
          headers.append('Authorization', 'Bearer ' + this.cookie.get('token'));
          return true;
      }
  }

  get(url) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(url, {
      headers: headers,
      withCredentials: true
    });
  }

  post(url, data) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.post(url, data, {
      headers: headers,
      withCredentials: true
    });
  }
  patch(url, data) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.patch(url, data, {
      headers: headers,
      withCredentials: true
    });
  }
  put(url, data) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(url, data, {
      headers: headers,
      withCredentials: true
    });
  }
  delete(url) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.delete(url, {
      headers: headers,
      withCredentials: true
    });
  }
}
