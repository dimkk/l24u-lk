export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Главная',
            icon: 'ion-android-home',
            selected: false,
            expanded: true,
            order: 0
          }
        },
        children: [
          {
          path: 'manage',
          data: {
            menu: {
                title: 'Управление',
                icon: 'fa fa-cubes'
              }
            }
          },
          // {
          // path: 'invites',
          // data: {
          //   menu: {
          //       title: 'Инвайты',
          //       icon: 'ion-person-add',
          //     }
          //   }
          // },
          {
          path: 'files',
          data: {
            menu: {
                title: 'Файлы',
                icon: 'ion-ios-folder'
              }
            }
          },
          {
          path: 'profile',
          data: {
            menu: {
                title: 'Профиль',
                icon: 'ion-person'
              }
            }
          },
        ]
      }
    ]
  }
];
