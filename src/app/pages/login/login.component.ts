import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../shared/auth/auth.service';
import { IAccount } from '../shared/auth/auth.interfaces';
import { AppSettings } from '../../app.settings';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';

import { GlobalState } from '../../global.state';

@Component({
  selector: 'login',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./login.scss')],
  template: require('./login.html'),
})
export class Login {

  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public showCaptcha: boolean = false;
  message: string;
  fails: number;
  disableButton: boolean;
  buttonMessage: string;
  capt: string;
  constructor(
    fb: FormBuilder,
    private service: AuthService,
    private router: Router,
    private _state: GlobalState
  ) {
    this.buttonMessage = 'Войти';
    this.fails = 0;
    this.disableButton = false;
    service.getUser();
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });
    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  public isDebug(){
    return AppSettings.DEBUG;
  }
  public handleCorrectCaptcha($event) {
    this.capt = $event;
  }
  public login() {
    this.buttonMessage = 'Входим..';
    this.disableButton = true;
      this.service.login(this.email.value, this.password.value, this.capt).subscribe(
        user => {
          this.service.setUserAndGoTo(user);
          this.fails = 0;
          this.buttonMessage = 'Вошли';
        },
        error => {
          this.message = error;
          this.fails++;
          this.buttonMessage = 'Войти';
          this.disableButton = false;
        }
      );
  }
}
