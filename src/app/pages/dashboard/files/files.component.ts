import { Component, ViewEncapsulation } from '@angular/core';
import { FilesService } from './files.service';

@Component({
  selector: 'files',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./files.scss')],
  template: require('./files.html')
})
export class Files {
  links: any[];
  icons = [
    'Download-Computer.svg',
    'Food-Dome.svg',
    'Television-Shelf.svg'
  ];
  texts = [
    'Установщик игрового клиента',
    'Папка System для игры',
    'Авто апдейтер'
  ]
  constructor(
    private files: FilesService
  ) {
    files.getData().subscribe(
      (data: any[]) => {
        this.links = data.map((v, i) => {
          return { icon: this.icons[i], link: v, text: this.texts[i]  };
        });
      }
    );
  }
  navigateTo(url){
      window.open(url, '_blank');
  }
}
