import { Injectable } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { HttpClient } from '../../shared/HttpClient';
import { IAccount } from '../../shared/auth/auth.interfaces';
import * as rx from 'rxjs';
import * as handler from '../../shared/errorHandler';
import { Router } from '@angular/router';
import { GlobalState } from '../../../global.state';

@Injectable()
export class FilesService {
  REQUEST_URL: string = AppSettings.API_ENDPOINT;
  REQUEST_URL_RAPI: string = this.REQUEST_URL + '/rapi';
  /**
   *
   */
  constructor(
    private http: HttpClient,
  ) {

  }

  getData(): rx.Observable<any> {
    return this.http.get(this.REQUEST_URL_RAPI + '/api/topic/1/1')
      .map((res) => {
        let body = res.json();
        let post = body.posts[0];
        let content = post.content;
        let links = [];
        $(content).find('a').each((i, v: any) => {
            links.push($(v).attr('href'));
        });
        return links;
      }).catch(handler.handleLoginError);
  };

}
