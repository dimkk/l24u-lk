import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { EmailValidator, EqualPasswordsValidator } from '../../../theme/validators';
import { AuthService } from '../../shared/auth/auth.service';
import { IAccount } from '../../shared/auth/auth.interfaces';
import { AppSettings } from '../../../app.settings';
import { GlobalState } from '../../../global.state';
import { AccountsService } from '../../admin/accounts/accounts.service';

@Component({
    selector: 'profile',
    encapsulation: ViewEncapsulation.None,
    styles: [require('./profile.scss')],
    template: require('./profile.html')
})
export class Profile {
    public form: FormGroup;
    public name: AbstractControl;
    public email: AbstractControl;
    public password: AbstractControl;
    public error: string;
    public submitted: boolean = false;
    message: string;

    constructor(
        fb: FormBuilder,
        private auth: AuthService,
        private state: GlobalState,
        private accounts: AccountsService
    ) {
        this.message = "Сохранить";
        this.error = '';
        this.form = fb.group({
            'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
            'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
            'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
        });

        this.name = this.form.controls['name'];
        this.email = this.form.controls['email'];
        this.password = this.form.controls['password'];

        if (auth.currentUser) {
            this.name.setValue(auth.currentUser.name);
            this.email.setValue(auth.currentUser.email);
            this.password.setValue(auth.currentUser.password);
        }

        state.subscribe('currentUser', (currentUser) => {
            this.name.setValue(currentUser.name);
            this.email.setValue(currentUser.email);
            this.password.setValue(currentUser.password);
        });
    }
    saveChanges() {
        this.message = 'Сохраняем..';
        if (this.form.valid) {
           let user = this.auth.currentUser;
           user.name = this.name.value;
           user.email = this.email.value;
           user.password = this.password.value;
        this.accounts.updateItem(user).subscribe(
            data => this.message = "Сохранили!",
            error => {
                this.error = error;
                this.message = "Ошибка";
                },
            () => {this.setTitle()}
            );
        }
    }
    setTitle(){
        setTimeout(() => {
            this.message = "Сохранить";
        }, 3000);
    }
}
