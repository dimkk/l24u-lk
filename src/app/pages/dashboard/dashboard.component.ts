import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dashboard',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./dashboard.scss')],
  template: `<router-outlet></router-outlet>`
})
export class Dashboard {

  constructor() {
  }

}
