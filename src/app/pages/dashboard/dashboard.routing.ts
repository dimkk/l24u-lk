import { Routes, RouterModule }  from '@angular/router';
import { AuthGuardUser } from '../shared/auth/auth.guard.user';
import { Dashboard } from './dashboard.component';
import { AppSettings } from '../../app.settings';
import { Manage } from './manage/manage.component';
import { Files } from './files/files.component';
import { Invites } from './invites/invites.component';
import { Profile } from './profile/profile.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Dashboard,
    canActivate: getActivate(),
    children: [
      { path: 'manage', component: Manage },
      { path: 'profile', component: Profile },
      { path: 'files', component: Files },
      { path: 'invites', component: Invites },
      { path: '', redirectTo: 'manage', pathMatch: 'full' }
      // { path: 'settings', component: Settings },
    ]
  }
];

function getActivate(){
  if (AppSettings.DEBUG) {
    return [AuthGuardUser];
  } else {
    return [AuthGuardUser]
  }
}

export const routing = RouterModule.forChild(routes);
