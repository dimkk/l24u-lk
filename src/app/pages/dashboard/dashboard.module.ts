import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';

import { Dashboard } from './dashboard.component';
import { routing }       from './dashboard.routing';

import { Manage } from './manage/manage.component';
import { Files } from './files/files.component';
import { Invites } from './invites/invites.component';
import { Profile } from './profile/profile.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    ReactiveFormsModule,
    Ng2SmartTableModule
  ],
  declarations: [
    Dashboard,
    Manage,
    Invites,
    Files,
    Profile
  ],
  providers: [
  ]
})
export default class DashboardModule {}
