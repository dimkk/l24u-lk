import { Component, ViewEncapsulation } from '@angular/core';

import { AccountsService } from './accounts.service';
import { LocalDataSource } from 'ng2-smart-table';
import { InvitesService, IInvite } from './invites.service';
import { AuthService } from '../../shared/auth/auth.service';
import { GlobalState } from '../../../global.state';

@Component({
  selector: 'invites',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./invites.scss')],
  template: require('./invites.html')
})
export class Invites {

  query: string = '';

  settings = {
    add: {
      addButtonContent: '<i class="ion-ios-plus-outline"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmCreate:true
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true
    },
    hideSubHeader: true,
    noDataMessage: 'У вас ещё нет инвайтов!',
    columns: {
      code: {
        title: 'code',
        type: 'string',
        editable: false,
        filter: false
      },
      inviteEmail: {
        title: 'email',
        type: 'string'
      },
      inviteMessage: {
        title: 'Сообщение',
        type: 'string'
      },
      inviteState: {
        title: 'Статус',
        type: 'string',
        editable: false
      }
    }
  };
  message: string;
  source: LocalDataSource = new LocalDataSource();
  getInvitesButtonMessage: string;
  sendInviteButtonMessage: string;
  invitesLeft: number;
  selectedRow: IInvite;

  constructor(
    private invites: InvitesService,
    private auth: AuthService,
    private state: GlobalState
  ) {
    this.getInvitesButtonMessage = 'Получить инвайт!';
    this.sendInviteButtonMessage = 'Отправить на почту!';
    this.invitesLeft = this.auth.currentUser ? this.auth.currentUser.invitesLeft : 0;
    this.invites.getData().subscribe(
          data => {
            this.source.load(data);
            if (data.length > 0) {
              this.selectedRow = data[0];
            }
          }
        );
    state.subscribe('currentUser', (user) => {
      this.invitesLeft = user.invitesLeft;
      this.invites.getData().subscribe(
          data => {
            this.source.load(data);
            if (data.length > 0) {
              this.selectedRow = data[0];
            }
          }
        );
    });
  }

  getInvite(){
    this.message = '';
     this.invites.creteItem({ inviteeId : '', inviteState: 'new'})
      .subscribe(
        invite => {
          this.source.append(invite);
          this.auth.getUserObs();
        },
        error => this.message = error
      );
  }

  onRowEdit(event){
    this.message = '';
    this.invites.updateItem(event.newData).subscribe(
      data => event.confirm.resolve(),
      error => this.message = error
    );
  }

  onRowSelect(event){
    this.message = '';
    this.selectedRow = event.data;
    //console.log(event);
  }

  onDeleteConfirm(event): void {
    this.message = '';
    if (window.confirm('Are you sure you want to delete?')) {
      this.invites.deleteItem(event.data).subscribe(
        data => event.confirm.resolve(),
        error => this.message = error
      );
    } else {
      event.confirm.reject();
    }
  }

  sendInvite(){
    this.message = '';
    if (!this.selectedRow) {
      this.message = 'Нужно выбрать строку!';
      return;
    }
    if (!this.selectedRow.inviteEmail) {
      this.message = 'Нужно указать Email чтобы отправить приглашение!';
      return;
    }
    if (this.selectedRow.inviteState === 'used') {
      this.message = 'Этот инвайт уже отправлен и использован';
      return;
    }
    this.invites.sendInvite(this.selectedRow).subscribe(
      data => {
        this.message = 'Сообщение отправлено!';
        this.source.update(this.selectedRow, data);
      },
      error => this.message = error
    );
  }
}
