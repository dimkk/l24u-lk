import { Injectable } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { HttpClient } from '../../shared/HttpClient';
import { AuthService } from '../../shared/auth/auth.service';
import * as rx from 'rxjs';
import * as handler from '../../shared/errorHandler';
import { Router } from '@angular/router';
import { GlobalState } from '../../../global.state';

@Injectable()
export class InvitesService {
  REQUEST_URL: string = AppSettings.API_ENDPOINT;
  REQUEST_URL_GETPOST: string = this.REQUEST_URL + '/api/invite';
  /**
   *
   */
  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) {

  }

  getData(): rx.Observable<IInvite[]> {
    let id = this.auth.currentUser ? this.auth.currentUser._id : '1';
    return this.http.get(this.REQUEST_URL_GETPOST + '?id=' + id )
      .map((res) => {
        let body = res.json();
        return body as IInvite[];
      });
  }

  updateItem(newInvite: IInvite) {
    return this.http.put(this.REQUEST_URL_GETPOST + '/' + newInvite._id, newInvite)
      .map((res) => {
        return res.json();
      })
      .catch(handler.handleLoginError);
  }
  creteItem(newInvite: IInvite) {
    newInvite.inviteeId = this.auth.currentUser._id;
    return this.http.post(this.REQUEST_URL_GETPOST, newInvite)
      .map((res) => {
        return res.json();
      })
      .catch(handler.handleLoginError);
  }
  deleteItem(newInvite: IInvite) {
    return this.http.delete(this.REQUEST_URL_GETPOST + '/' + newInvite._id)
      .map((res) => {
        if (res.status !== 200)
        throw new Error('delete failed');
      })
      .catch(handler.handleLoginError);
  }
  sendInvite(newInvite: IInvite) {
    return this.http.put(this.REQUEST_URL_GETPOST + '/' + newInvite._id + '/send', newInvite)
      .map((res) => {
        if (res.status !== 200) {
          throw new Error('sync failed');
        }
        let body = res.json();
        return body as IInvite[];
      }).catch(handler.handleLoginError);
  }
}


export interface IInvite {
    _id?: string;
    code?: string;
    inviteeId: string;
    inviteEmail?: string;
    inviteMessage?: string;
    inviteState: 'new' | 'sent' | 'used' | 'expired';
}