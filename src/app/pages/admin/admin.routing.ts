import { Routes, RouterModule }  from '@angular/router';
import { AuthGuardAdmin } from '../shared/auth/auth.guard.admin';
import { Admin } from './admin.component';
import { Accounts } from './accounts/accounts.component';
import { Settings } from './settings/';
import { AppSettings } from '../../app.settings';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Admin,
    canActivate: getActivate(),
    children: [
      { path: 'accounts', component: Accounts },
      { path: 'settings', component: Settings },
    ]
  }
];

function getActivate(){
  if (AppSettings.DEBUG) {
    return [AuthGuardAdmin];
  } else {
    return [AuthGuardAdmin];
  }
}

export const routing = RouterModule.forChild(routes);
