import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { Admin } from './admin.component';
import { routing }       from './admin.routing';

import { Ng2SmartTableModule } from 'ng2-smart-table';

import { Accounts } from './accounts/';
import { Settings } from './settings/';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    Ng2SmartTableModule
  ],
  declarations: [
    Admin,
    Accounts,
    Settings
  ],
  providers: [
  ]
})
export default class AdminModule {}
