export const ADMIN_MENU = [
    {
        path: 'admin',
        data: {
            menu: {
                title: 'Админка',
                icon: 'ion-key',
                selected: false,
                expanded: true,
                order: 0
            }
        },
        children: [
          {
            path: 'accounts',
            data: {
              menu: {
                title: 'Аккаунты',
                icon: 'ion-person-stalker'
              }
            }
          },
          // {
          //   path: 'settings',
          //   data: {
          //     menu: {
          //       title: 'Настройки',
          //       icon: 'ion-settings'
          //     }
          //   }
          // }
        ]
    }
];
