import { Component, ViewEncapsulation, HostBinding } from '@angular/core';
import { slideInDownAnimation } from './admin.animations';

@Component({
  selector: 'admin',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./admin.scss')],
  template: '<router-outlet></router-outlet>',
  animations: [ slideInDownAnimation ],
  host: {
    '@routeAnimation': 'true',
    'style.display': 'block',
    'style.position': 'absolute'
  }
})
export class Admin {

  constructor() {
  }

}
