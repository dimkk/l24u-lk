import { Component, ViewEncapsulation } from '@angular/core';

import { AccountsService } from '../accounts/accounts.service';
import { AuthService } from '../../shared/auth/auth.service';

@Component({
  selector: 'settings',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./settings.scss')],
  template: require('./settings.html')
})
export class Settings {
  message: string;
  constructor(
    protected service: AccountsService,
    private auth: AuthService
  ) {
      this.message = "Го";
  }
  syncNow(){
      this.message = 'Думаем';
      this.service.syncDb().subscribe(
          data => this.message = 'ok!',
          err => this.message = 'err'
      );
  }
}
