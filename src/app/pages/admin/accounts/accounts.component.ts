import { Component, ViewEncapsulation } from '@angular/core';

import { AccountsService } from './accounts.service';
import { LocalDataSource } from 'ng2-smart-table';
import { AuthService } from '../../shared/auth/auth.service';

@Component({
  selector: 'basic-tables',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./accounts.scss')],
  template: require('./accounts.html')
})
export class Accounts {

  query: string = '';

  settings = {
    add: {
      addButtonContent: '<i class="ion-ios-plus-outline"></i>',
      createButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmCreate:true
    },
    edit: {
      editButtonContent: '<i class="ion-edit"></i>',
      saveButtonContent: '<i class="ion-checkmark"></i>',
      cancelButtonContent: '<i class="ion-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true
    },
    columns: {
      name: {
        title: 'name',
        type: 'string',
        editable: false
      },
      email: {
        title: 'email',
        type: 'string'
      },
      role: {
        title: 'role',
        type: 'string'
      },
      invitesLeft: {
        title: 'Количество инвайтов',
        type: 'number',
        editable: true
      },
      password: {
        title: 'password',
        type: 'string',
        editable: true
      }
    }
  };
  message: string;
  source: LocalDataSource = new LocalDataSource();

  constructor(
    protected service: AccountsService,
    private auth: AuthService
  ) {
    this.message = '';
    this.service.getData().subscribe(
      data => this.source.load(data)
    );
  }

  onRowCreate(event){
    this.message = '';
     this.auth.registerUser(event.newData, false).subscribe(
        user => event.confirm.resolve(),
        error => this.message = error
      );
  }

  onRowEdit(event){
    this.message = '';
    this.service.updateItem(event.newData).subscribe(
      data => event.confirm.resolve(),
      error => this.message = error
    );
  }

  onDeleteConfirm(event): void {
    this.message = '';
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteItem(event.data).subscribe(
        data => event.confirm.resolve(),
        error => this.message = error
      );
    } else {
      event.confirm.reject();
    }
  }
}
