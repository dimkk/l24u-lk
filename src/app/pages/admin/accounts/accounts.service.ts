import { Injectable } from '@angular/core';
import { AppSettings } from '../../../app.settings';
import { HttpClient } from '../../shared/HttpClient';
import { IAccount } from '../../shared/auth/auth.interfaces';
import * as rx from 'rxjs';
import * as handler from '../../shared/errorHandler';
import { Router } from '@angular/router';
import { GlobalState } from '../../../global.state';

@Injectable()
export class AccountsService {
  REQUEST_URL: string = AppSettings.API_ENDPOINT;
  REQUEST_URL_GETPOST: string = this.REQUEST_URL + '/api/users';
  REQUEST_URL_ADMIN_CREATE: string = this.REQUEST_URL + '/auth/local/admin';
  /**
   *
   */
  constructor(
    private http: HttpClient,
  ) {

  }

  getData(): rx.Observable<IAccount[]> {
    return this.http.get(this.REQUEST_URL_GETPOST)
      .map((res) => {
        let body = res.json();
        return body as IAccount[];
      });
  }

  updateItem(newAcc: IAccount) {
    return this.http.post(this.REQUEST_URL_GETPOST + '/' + newAcc._id, newAcc)
      .map((res) => {
        if (res.status !== 200)
        throw new Error('update failed');
      })
      .catch(handler.handleLoginError);
  }
  deleteItem(newAcc: IAccount) {
    return this.http.delete(this.REQUEST_URL_GETPOST + '/' + newAcc._id)
      .map((res) => {
        if (res.status !== 200)
        throw new Error('delete failed');
      })
      .catch(handler.handleLoginError);
  }
  syncDb() {
    return this.http.get(this.REQUEST_URL_GETPOST + '/sync')
      .map((res) => {
        if (res.status !== 200) {
          throw new Error('sync failed');
        }
      }).catch(handler.handleLoginError);
  }
}
