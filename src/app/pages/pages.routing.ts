import { Routes, RouterModule }  from '@angular/router';
import { Pages } from './pages.component';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => System.import('./login/login.module')
  },
  {
    path: 'register',
    loadChildren: () => System.import('./register/register.module')
  },
  {
    path: 'pass',
    loadChildren: () => System.import('./pass/pass.module')
  },
  {
    path: 'pages',
    component: Pages,
    children: [
      { path: 'admin', loadChildren: () => System.import('./admin/admin.module') },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: () => System.import('./dashboard/dashboard.module') },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
